const WebPageTest = require('webpagetest')

const allowedTestcases = ['monolith', 'single-spa', 'module-federation', '1', '2', '3']

printUsageInfoAndExit = () => {
    console.info('Usage:\tserver.js <testcase>')
    console.info('\ttestcase: [1: monolith, 2: single-spa, 3: module-federation]')
    process.exit(-1)
}

if (process.argv.length < 3) {
    printUsageInfoAndExit()
}
const args = process.argv.slice(2);
const requestedTestcase = args[0]

if (!allowedTestcases.includes(requestedTestcase)) {
    printUsageInfoAndExit()
}

const singleSpaUrl = 'http://35.234.97.149/'
const angularMonolithUrl = 'http://35.242.206.166/'
const moduleFederationUrl = 'http://34.89.225.232/'

let url = ''
switch (requestedTestcase) {
    case '1':
    case 'monolith':
        url = angularMonolithUrl
        break
    case '2':
    case 'single-spa':
        url = singleSpaUrl
        break
    case '3':
    case 'module-federation':
        url = moduleFederationUrl
        break
    default:
        printUsageInfoAndExit()
}

const serverUrl = 'http://35.198.184.35/'
const apiKey = 'vOdM3vMQiim4DWK5CqCFVQPVlzvFDXbk'

const wpt = new WebPageTest(serverUrl, apiKey)

let scripts = []


const navigateToHome = [
    {setEventName: 'Load page'},
    {navigate: url}
]
let clickLogin = []
if (url === singleSpaUrl) {
    clickLogin = [
        {setEventName: 'Navigate to Login page'},
        {execAndWait: 'document.getElementsByTagName("authentication-user-panel")[0].shadowRoot.children[2].children[0].children[0].children[0].click()'}
    ]
} else {
    clickLogin = [
        {setEventName: 'Navigate to Login page'},
        {execAndWait: 'document.getElementsByTagName("a")[1].click()'}
    ]
}

const login = [
    {setEventName: 'Login'},
    {exec: 'const updateInputValue = (input, newValue) => {  let lastValue = input.value;  input.value = newValue;  let event = new Event("input", { bubbles: true });  let tracker = input._valueTracker;  if (tracker) {  tracker.setValue(lastValue);  }  input.dispatchEvent(event);};'},
    {exec: 'const [usernameInput, passwordInput] = document.getElementsByTagName("input")'},
    {exec: 'updateInputValue(usernameInput, "Test")'},
    {exec: 'updateInputValue(passwordInput, "password")'},
    {execAndWait: 'document.getElementsByTagName("button")[0].click()'}
]

const article = [
    {setEventName: 'Navigate to Article Administration'},
    {execAndWait: 'document.getElementsByTagName("a")[2].click()'}
]

const sales = [
    {setEventName: 'Navigate to Sales Administration'},
    {execAndWait: 'document.getElementsByTagName("a")[3].click()'}
]

    
scripts = scripts.concat(navigateToHome)
scripts = scripts.concat(clickLogin)
scripts = scripts.concat(login)
scripts = scripts.concat(article)
scripts = scripts.concat(sales)
scripts = scripts.concat('waitForComplete')



const script = wpt.scriptToString(scripts)

const options = {
    connectivity: 'Cable',
    firstViewOnly: false,
    runs: 3,
    pollResults: 5,
    location: 'gce-europe-west3-linux:Chrome'
}

console.info('Running tests at ' + url + ' ...')

wpt.runTest(script,
    options,
    (err, result) => {
        if (err) {
            console.error('Test execution failed.')
            throw err
        }
        console.log('Result:', serverUrl + '/result/' + result.data.id)
        console.log('Load time:', result.data.average.firstView.loadTime)
        console.log('First byte:', result.data.average.firstView.TTFB)
        console.log('Start render:', result.data.average.firstView.render)
        console.log('Speed Index:', result.data.average.firstView.SpeedIndex)
        console.log('DOM elements:', result.data.average.firstView.domElements)

        console.log('(Doc complete) Requests:', result.data.average.firstView.requestsDoc)
        console.log('(Doc complete) Bytes in:', result.data.average.firstView.bytesInDoc)

        console.log('(Fully loaded) Time:', result.data.average.firstView.fullyLoaded)
        console.log('(Fully loaded) Requests:', result.data.average.firstView.requestsFull)
        console.log('(Fully loaded) Bytes in:', result.data.average.firstView.bytesIn)
        console.info('Test execution completed successfully')
    })


